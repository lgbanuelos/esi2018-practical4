package com.example.rentit.sales.web.controllers;

import com.example.rentit.sales.domain.models.PurchaseOrder;
import com.example.rentit.sales.application.dto.CatalogQueryDTO;
import com.example.rentit.sales.application.services.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dashboard")
public class	DashboardController	{
    @Autowired
    SalesService salesService;

    @GetMapping("/catalog/form")
    public String getQueryForm(Model model)	{
        model.addAttribute("catalogQuery",	new CatalogQueryDTO());
        return	"dashboard/catalog/query-form";
    }
    @PostMapping("/catalog/query")
    public String executeQuery(CatalogQueryDTO query, Model model) {
        model.addAttribute("plants", salesService.queryPlantCatalog(null, null));
        model.addAttribute("po", new PurchaseOrder());
        return "dashboard/catalog/query-result";
    }
}