package com.example.rentit.sales.domain.models;

public enum EquipmentCondition {
    SERVICEABLE, UNSERVICEABLE_REPARABLE
}
