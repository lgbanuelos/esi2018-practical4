package com.example.rentit.sales.domain.models;

public enum POStatus { PENDING, REJECTED, OPEN, CLOSED }
