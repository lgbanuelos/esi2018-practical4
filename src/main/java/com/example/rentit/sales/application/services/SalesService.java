package com.example.rentit.sales.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.domain.models.PlantInventoryEntry;
import com.example.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesService {
    @Autowired
    PlantInventoryEntryRepository repo;

    public List<PlantInventoryEntry> queryPlantCatalog(String name, BusinessPeriodDTO rentalPeriod) {
        return repo.findAll();
    }
}
