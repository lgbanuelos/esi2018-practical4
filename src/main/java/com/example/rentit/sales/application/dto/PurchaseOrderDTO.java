package com.example.rentit.sales.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.sales.domain.models.POStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@NoArgsConstructor(force=true)
public class PurchaseOrderDTO {
    Long id;
    PlantInventoryEntryDTO plant;
    @Column(precision = 8, scale = 2)
    BigDecimal total;
    POStatus status;
    BusinessPeriodDTO rentalPeriod;
}
