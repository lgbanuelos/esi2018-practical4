package com.example.rentit.inventory.domain.repositories;

import com.example.rentit.inventory.domain.models.PlantInventoryEntry;

import java.time.LocalDate;
import java.util.List;

public interface CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
}
