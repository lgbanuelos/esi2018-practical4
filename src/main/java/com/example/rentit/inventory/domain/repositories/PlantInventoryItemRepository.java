package com.example.rentit.inventory.domain.repositories;

import com.example.rentit.inventory.domain.models.PlantInventoryEntry;
import com.example.rentit.inventory.domain.models.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {
    PlantInventoryItem findOneByPlantInfo(PlantInventoryEntry entry);
}
