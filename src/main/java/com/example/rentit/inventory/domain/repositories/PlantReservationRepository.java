package com.example.rentit.inventory.domain.repositories;

import com.example.rentit.inventory.domain.models.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long> {
}
